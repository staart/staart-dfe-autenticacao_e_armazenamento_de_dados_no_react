import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useAuth } from "../context/authContext";

export const ForgotPassword =() =>{
  const navigate = useNavigate();
  const {resetPassword} = useAuth();
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);

  async function handleSubmit(element){
    element.preventDefault();

    setLoading(true);

    try{
      await resetPassword(email);
      alert("recovery email sent")
      navigate("/login");
  }catch(error){
      alert("Error when changing password");
  }
  setLoading(false);
  }

  return(
  <div className="container">
    <h1>Forgot Password</h1>
    <form onSubmit={handleSubmit}>
      <label>Email</label>
      <input type="email"value={email} onChange={(element) => setEmail(element.target.value)} />
      <button disabled={loading} className="button-block">Reset password</button>
    </form>
    <div className="center">
      <div>
      <p>Already have an account?<Link to="/login"> Log in</Link></p>
      <p>Don't have an account?<Link to="/signup"> Sign up</Link></p>
      </div>
    </div>
  </div>
  )
}