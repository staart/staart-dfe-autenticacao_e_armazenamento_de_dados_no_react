import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useAuth } from "../context/authContext";

export const Login = () => {     
    const {signIn} = useAuth();
    const navigate = useNavigate();
    const [email, setEmail] = useState("");     
    const [password, setPassword] = useState("");    
    const [loading, setLoading] = useState(false);

    async function handleSubmit(element){
        element.preventDefault();
        setLoading(true);

        if(password.length < 6 ){
            alert("Password must be at least 6 characters long");
            setLoading(false);
            return;
        }

        try {
            await signIn(email, password);
            navigate("/");
        } catch (error) {
            alert("Error trying to login")
        }

        setLoading(false);
    }

    return (
        <div className="container">
            <h2>Log in</h2>
            <form onSubmit={handleSubmit} >
                <label>Email</label>
                <input type="email" value={email} onChange={(element) => setEmail(element.target.value)} />

                <label>Password</label>
                <input type="password" value={password} onChange={(element) => setPassword(element.target.value)} />

                <button disabled={loading} className="button-block" type="submit">Log in</button>
            </form>
            <div className="center">
      <div>
      <p>Forgot password?<Link to="/forgot-password"> Reset password</Link></p>
      <p>create new account?<Link to="/signup"> Sign up</Link></p>
      </div>
    </div>
        </div>
    );
};