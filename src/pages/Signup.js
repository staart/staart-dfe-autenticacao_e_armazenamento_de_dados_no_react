import { async } from "@firebase/util";
import React, { useState } from "react";     
import { useAuth } from "../context/authContext";
import { Link } from "react-router-dom";

export const Signup =() =>{      
    const {signUp} = useAuth();      
    const [email, setEmail] = useState("");      
    const [password, setPassword] = useState("");    
    const [confirmPassword, setConfirmPassword] = useState("");  
    const [loading, setLoading] = useState(false);
    
    async function handleSubmit(element){
        element.preventDefault();
        setLoading(true);

        if(password.length < 6 ){
            alert("Password must be at least 6 characters long");
            setLoading(false);
            return;
        }

        if(password !== confirmPassword){
            alert("Passwords don't check")
            setLoading(false);
        }

        try {
            await signUp(email, password);
        } catch (error) {
            alert("Error when trying to create user")
        }
        setLoading(false);
    }

    return(
        <div className="container">
            <h2>Sign Up</h2>
            <form onSubmit={handleSubmit} >
                <label>Email</label>
                <input type="email" value={email} onChange={(element) => setEmail(element.target.value)} />

                <label>Password</label>
                <input type="password" value={password} onChange={(element) => setPassword(element.target.value)} />

                <label>Confirm password</label>
                <input type="password" value={confirmPassword} onChange={(element) => setConfirmPassword(element.target.value)}/>

                <button disabled={loading} className="button-block" type="submit">Sign up</button>
            </form>

            <div className="center">
      <div>
      <p>Already have an account?<Link to="/login"> Log in</Link></p>
      </div>
    </div>
        </div>
    );
}