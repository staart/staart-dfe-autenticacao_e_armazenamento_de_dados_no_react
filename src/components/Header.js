import React from "react";
//import logo from "../images/logo.png"

export const Header = () => {
  return(
    <nav>
      <div className="container">
        <a className="navigation-brand" href="/">
          <img src="https://staart.com/assets/svg/logo-staart.svg" alt="Staart"/>
        </a>
      </div>
    </nav>
  )
}